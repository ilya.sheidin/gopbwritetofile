#Install protoc
* https://github.com/protocolbuffers/protobuf/releases
* unzip protoc-* -d protoc
* chmod 755 -R protoc
* BASE=/usr/local
* sudo rm -rf $BASE/include/google/protobuf/
* sudo cp protoc/bin/protoc $BASE/bin 
* sudo cp -R protoc/include/* $BASE/include 
* go get -u github.com/golang/protobuf/protoc-gen-go