package main

import (
	"fmt"
	"github.com/golang/protobuf/jsonpb"
	"github.com/golang/protobuf/proto"
	complexpb "go-proto/src/complex"
	enumpb "go-proto/src/enum"
	simplepb "go-proto/src/simple"
	"io/ioutil"
	"log"
)

func main() {
	sm := doSimple()
	readAndWriteDemo(sm)
	jsonString := convertToJson(sm)
	convertFromJson(jsonString, sm)
	fmt.Println(sm)
	/*Enums*/
	enumMessage()
	complexMessage()
}

func complexMessage() {
	cm := complexpb.Building{
		BuildingName:   "Yafe",
		BuildingNumber: 14,
		Street: &complexpb.Street{
			StreetName: "Ziporen 11",
			City: &complexpb.City{
				Name:        "Rishon Lezion",
				ZipCode:     "11111",
				CountryName: "Israel",
			},
		},
	}
	fmt.Println(cm)
}

func enumMessage() {
	em := enumpb.MyDayOfTheWeek{
		Id:           35,
		DayOfTheWeek: enumpb.DayOfTheWeek_SUNDAY,
	}
	em.DayOfTheWeek = enumpb.DayOfTheWeek_WEDNESDAY
	fmt.Println(em)
}

func writeToFile(fName string, pb proto.Message) error {
	fContent, err := proto.Marshal(pb)
	if err != nil {
		log.Fatal("Can't serialise to bytes", err)
		return err
	}

	err = ioutil.WriteFile(fName, fContent, 0644)
	if err != nil {
		log.Fatal("Can't write to file", err)
		return err
	}
	fmt.Println("Successfully written to file", fName)
	return nil
}

func readFromFile(fName string, pb proto.Message) error {
	file, err := ioutil.ReadFile(fName)
	if err != nil {
		log.Fatal("Failed to read from file", err)
		return err
	}
	err = proto.Unmarshal(file, pb)
	if err != nil {
		log.Fatal("Failed to create proto buffer", err)
		return err
	}

	return nil
}

func doSimple() *simplepb.SimpleMessage {
	sm := simplepb.SimpleMessage{
		Id:         546,
		IsSimple:   true,
		Name:       "Ilya",
		SampleList: []int32{1, 4, 7},
	}
	return &sm
}

func readAndWriteDemo(sm proto.Message) {
	/*Generate protocol buffer with pointer*/

	/*write serialised struct to bin file*/
	err := writeToFile("simple.bin", sm)
	if err != nil {
		log.Fatal("Error:", err)
	}
	/*Create empty struct with pointer for file*/
	simpleMessage := &simplepb.SimpleMessage{}
	err = readFromFile("simple.bin", simpleMessage)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(simpleMessage)
}

func convertToJson(pb proto.Message) string {
	marshaller := jsonpb.Marshaler{}
	str, err := marshaller.MarshalToString(pb)
	if err != nil {
		log.Fatalln(err)
		return ""
	}
	return str
}

func convertFromJson(message string, pb proto.Message) bool {
	err := jsonpb.UnmarshalString(message, pb)
	if err != nil {
		log.Println(err)
		return false
	}
	return true
}
